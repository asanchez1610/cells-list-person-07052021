import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../cells-list-person-capa.js';

suite('CellsListPersonCapa', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-list-person-capa></cells-list-person-capa>`);
    await el.updateComplete;
  });

  test('instantiating the element with default properties works', () => {
    const element = el.shadowRoot.querySelector('p');
    assert.equal(element.innerText, 'Welcome to Cells');
  });

});
