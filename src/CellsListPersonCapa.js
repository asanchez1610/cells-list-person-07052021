import {  html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsListPersonCapa-styles.js';
import { BaseElement } from '@bbva-commons-web-components/cells-base-elements/BaseElement';
import buttonsCSS from '@bbva-commons-web-components/cells-base-elements/ButtonsCss';
import '@bbva-commons-web-components/cells-data-table/cells-data-table'
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-list-person-capa></cells-list-person-capa>
```

##styling-doc

@customElement cells-list-person-capa
*/
export class CellsListPersonCapa extends BaseElement {
  static get is() {
    return 'cells-list-person-capa';
  }

  // Declare properties
  static get properties() {
    return {
  
        columns: Array,
        persons: Array

    };
  }

  // Initialize properties
  constructor() {
    super();
    this.columns = [];
    this.persons = [];
  }

  static get styles() {
    return [
      styles,
      buttonsCSS,
      getComponentSharedStyles('cells-list-person-capa-shared-styles')
    ];
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <cells-data-table 
                  cls="dark" 
                  .columnsConfig="${this.columns}"
                  checkbox-selection
                  .items="${this.persons}"
                  ></cells-data-table>
    `;
  }
}
